# The Grinder 4.0.0
# HTTP script recorded by TCPProxy at Dec 31, 2018 4:59:13 PM

from net.grinder.script import Test
from net.grinder.script.Grinder import grinder
from net.grinder.plugin.http import HTTPPluginControl, HTTPRequest
from HTTPClient import NVPair
connectionDefaults = HTTPPluginControl.getConnectionDefaults()
httpUtilities = HTTPPluginControl.getHTTPUtilities()

# To use a proxy server, uncomment the next line and set the host and port.
# connectionDefaults.setProxyServer("localhost", 8001)

def createRequest(test, url, headers=None):
    """Create an instrumented HTTPRequest."""
    request = HTTPRequest(url=url)
    if headers: request.headers=headers
    test.record(request, HTTPRequest.getHttpMethodFilter())
    return request

# These definitions at the top level of the file are evaluated once,
# when the worker process is started.

connectionDefaults.defaultHeaders = \
  [ NVPair('User-Agent', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.14; rv:64.0) Gecko/20100101 Firefox/64.0'),
    NVPair('Accept-Language', 'en-GB,en;q=0.5'), ]

headers0= \
  [ NVPair('Accept', 'application/json, text/javascript, */*; q=0.01'),
    NVPair('Accept-Encoding', 'gzip, deflate'),
    NVPair('Referer', 'http://192.168.99.100:6373/'), ]

headers1= \
  [ NVPair('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'),
    NVPair('Accept-Encoding', 'gzip, deflate'),
    NVPair('Cache-Control', 'no-cache'), ]

headers2= \
  [ NVPair('Accept', 'text/css,*/*;q=0.1'),
    NVPair('Accept-Encoding', 'gzip, deflate'),
    NVPair('Referer', 'http://172.18.130.34:8000/'),
    NVPair('Cache-Control', 'no-cache'), ]

headers3= \
  [ NVPair('Accept', '*/*'),
    NVPair('Accept-Encoding', 'gzip, deflate'),
    NVPair('Referer', 'http://172.18.130.34:8000/'),
    NVPair('Cache-Control', 'no-cache'), ]

headers4= \
  [ NVPair('Accept', 'application/font-woff2;q=1.0,application/font-woff;q=0.9,*/*;q=0.8'),
    NVPair('Accept-Encoding', 'identity'),
    NVPair('Referer', 'http://cdn.componentator.com/'),
    NVPair('Cache-Control', 'no-cache'), ]

headers5= \
  [ NVPair('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'),
    NVPair('Accept-Encoding', 'gzip, deflate'),
    NVPair('Referer', 'http://172.18.130.34:8000/'),
    NVPair('Cache-Control', 'no-cache'), ]

headers6= \
  [ NVPair('Accept', '*/*'),
    NVPair('Accept-Encoding', 'gzip, deflate'),
    NVPair('Referer', 'http://172.18.130.34:8000/account/'),
    NVPair('Cache-Control', 'no-cache'), ]

headers7= \
  [ NVPair('Accept', 'text/css,*/*;q=0.1'),
    NVPair('Accept-Encoding', 'gzip, deflate'),
    NVPair('Referer', 'http://172.18.130.34:8000/account/'),
    NVPair('Cache-Control', 'no-cache'), ]

url0 = 'http://192.168.99.100:6373'
url1 = 'http://172.18.130.34:8000'
url2 = 'http://cdn.componentator.com:80'

request101 = createRequest(Test(101, 'GET data-latest'), url0, headers0)

request201 = createRequest(Test(201, 'GET status'), url0, headers0)

request301 = createRequest(Test(301, 'GET status'), url0, headers0)

request401 = createRequest(Test(401, 'GET data-latest'), url0, headers0)

request501 = createRequest(Test(501, 'GET /'), url1, headers1)

request502 = createRequest(Test(502, 'GET widgets.css'), url1, headers2)

request503 = createRequest(Test(503, 'GET default.css'), url1, headers2)

request504 = createRequest(Test(504, 'GET default.js'), url1, headers3)

request505 = createRequest(Test(505, 'GET widgets.js'), url1, headers3)

request601 = createRequest(Test(601, 'GET spa.min@14.css'), url2, headers2)

request602 = createRequest(Test(602, 'GET spa.min@14.js'), url2, headers3)

request701 = createRequest(Test(701, 'GET data-latest'), url0, headers0)

request801 = createRequest(Test(801, 'GET status'), url0, headers0)

request901 = createRequest(Test(901, 'GET /'), url1, headers3)

request902 = createRequest(Test(902, 'GET 1611262350T17n4p.jpg'), url1, headers3)

request903 = createRequest(Test(903, 'GET 1611271010Tsbkwf.jpg'), url1, headers3)

request904 = createRequest(Test(904, 'GET 1611262355Tkl91a.jpg'), url1, headers3)

request905 = createRequest(Test(905, 'GET logo.png'), url1, headers3)

request906 = createRequest(Test(906, 'GET 1611271007Twyhfp.jpg'), url1, headers3)

request907 = createRequest(Test(907, 'GET 1611271002T96ob6.jpg'), url1, headers3)

request908 = createRequest(Test(908, 'GET 1611270951T7psal.jpg'), url1, headers3)

request909 = createRequest(Test(909, 'GET 1611270954Tyii51.jpg'), url1, headers3)

request910 = createRequest(Test(910, 'GET 1611270945Tkp1cf.jpg'), url1, headers3)

request911 = createRequest(Test(911, 'GET 1611270925T1hqh9.jpg'), url1, headers3)

request912 = createRequest(Test(912, 'GET 1611270935Tzzei5.jpg'), url1, headers3)

request913 = createRequest(Test(913, 'GET 1611270920Tdyznx.jpg'), url1, headers3)

request914 = createRequest(Test(914, 'GET 1611270915Tq9ceo.jpg'), url1, headers3)

request915 = createRequest(Test(915, 'GET 1611270939Tnr479.jpg'), url1, headers3)

request916 = createRequest(Test(916, 'GET 1611270911Tm8m2j.jpg'), url1, headers3)

request1001 = createRequest(Test(1001, 'GET data-latest'), url0, headers0)

request1101 = createRequest(Test(1101, 'GET status'), url0, headers0)

request1201 = createRequest(Test(1201, 'GET fontawesome-webfont.woff2'), url2, headers4)

request1301 = createRequest(Test(1301, 'GET favicon.ico'), url1, headers1)

request1401 = createRequest(Test(1401, 'GET status'), url0, headers0)

request1501 = createRequest(Test(1501, 'GET data-latest'), url0, headers0)

request1601 = createRequest(Test(1601, 'GET /'), url1, headers5)

request1602 = createRequest(Test(1602, 'GET default.js'), url1, headers6)

request1603 = createRequest(Test(1603, 'GET widgets.css'), url1, headers7)

request1604 = createRequest(Test(1604, 'GET widgets.js'), url1, headers6)

request1605 = createRequest(Test(1605, 'GET default.css'), url1, headers7)

request1701 = createRequest(Test(1701, 'GET spa.min@14.css'), url2, headers2)

request1702 = createRequest(Test(1702, 'GET spa.min@14.js'), url2, headers3)

request1801 = createRequest(Test(1801, 'GET status'), url0, headers0)

request1901 = createRequest(Test(1901, 'GET data-latest'), url0, headers0)

request2001 = createRequest(Test(2001, 'GET /'), url1, headers6)

request2002 = createRequest(Test(2002, 'GET logo.png'), url1, headers6)

request2101 = createRequest(Test(2101, 'GET fontawesome-webfont.woff2'), url2, headers4)

request2201 = createRequest(Test(2201, 'GET favicon.ico'), url1, headers1)

request2301 = createRequest(Test(2301, 'GET status'), url0, headers0)

request2401 = createRequest(Test(2401, 'GET data-latest'), url0, headers0)

request2501 = createRequest(Test(2501, 'GET status'), url0, headers0)

request2601 = createRequest(Test(2601, 'GET data-latest'), url0, headers0)

request2701 = createRequest(Test(2701, 'GET status'), url0, headers0)

request2801 = createRequest(Test(2801, 'GET data-latest'), url0, headers0)

request2901 = createRequest(Test(2901, 'GET status'), url0, headers0)

request3001 = createRequest(Test(3001, 'GET data-latest'), url0, headers0)

request3101 = createRequest(Test(3101, 'GET status'), url0, headers0)

request3201 = createRequest(Test(3201, 'GET data-latest'), url0, headers0)

request3301 = createRequest(Test(3301, 'GET status'), url0, headers0)

request3401 = createRequest(Test(3401, 'GET data-latest'), url0, headers0)

request3501 = createRequest(Test(3501, 'GET status'), url0, headers0)

request3601 = createRequest(Test(3601, 'GET data-latest'), url0, headers0)

request3701 = createRequest(Test(3701, 'GET status'), url0, headers0)

request3801 = createRequest(Test(3801, 'GET data-latest'), url0, headers0)

request3901 = createRequest(Test(3901, 'GET status'), url0, headers0)

request4001 = createRequest(Test(4001, 'GET data-latest'), url0, headers0)

request4101 = createRequest(Test(4101, 'GET status'), url0, headers0)

request4201 = createRequest(Test(4201, 'GET data-latest'), url0, headers0)

request4301 = createRequest(Test(4301, 'POST /'), url1, headers6)

request4401 = createRequest(Test(4401, 'GET /'), url1, headers5)

request4402 = createRequest(Test(4402, 'GET widgets.css'), url1, headers7)

request4403 = createRequest(Test(4403, 'GET default.css'), url1, headers7)

request4404 = createRequest(Test(4404, 'GET widgets.js'), url1, headers6)

request4405 = createRequest(Test(4405, 'GET default.js'), url1, headers6)

request4501 = createRequest(Test(4501, 'GET status'), url0, headers0)

request4601 = createRequest(Test(4601, 'GET data-latest'), url0, headers0)

request4701 = createRequest(Test(4701, 'GET spa.min@14.css'), url2, headers2)

request4702 = createRequest(Test(4702, 'GET spa.min@14.js'), url2, headers3)

request4801 = createRequest(Test(4801, 'GET logo.png'), url1, headers6)

request4901 = createRequest(Test(4901, 'GET /'), url1, headers6)

request5001 = createRequest(Test(5001, 'GET fontawesome-webfont.woff2'), url2, headers4)

request5101 = createRequest(Test(5101, 'GET favicon.ico'), url1, headers1)

request5201 = createRequest(Test(5201, 'GET data-latest'), url0, headers0)

request5301 = createRequest(Test(5301, 'GET status'), url0, headers0)

request5401 = createRequest(Test(5401, 'GET data-latest'), url0, headers0)

request5501 = createRequest(Test(5501, 'GET status'), url0, headers0)

request5601 = createRequest(Test(5601, 'GET status'), url0, headers0)

request5701 = createRequest(Test(5701, 'GET data-latest'), url0, headers0)

request5801 = createRequest(Test(5801, 'GET status'), url0, headers0)

request5901 = createRequest(Test(5901, 'GET data-latest'), url0, headers0)


class TestRunner:
  """A TestRunner instance is created for each worker thread."""

  # A method for each recorded page.
  def page1(self):
    """GET data-latest (request 101)."""
    result = request101.GET('/recording/data-latest')

    return result

  def page2(self):
    """GET status (request 201)."""
    result = request201.GET('/agents/status')

    return result

  def page3(self):
    """GET status (request 301)."""
    result = request301.GET('/agents/status')

    return result

  def page4(self):
    """GET data-latest (request 401)."""
    result = request401.GET('/recording/data-latest')

    return result

  def page5(self):
    """GET / (requests 501-505)."""
    result = request501.GET('/')

    grinder.sleep(77)
    self.token_ts = \
      '115ga'
    request502.GET('/widgets.css' +
      '?ts=' +
      self.token_ts)

    request503.GET('/default/css/default.css')

    request504.GET('/default/js/default.js')

    request505.GET('/widgets.js' +
      '?ts=' +
      self.token_ts)

    return result

  def page6(self):
    """GET spa.min@14.css (requests 601-602)."""
    result = request601.GET('/spa.min@14.css')

    request602.GET('/spa.min@14.js')

    return result

  def page7(self):
    """GET data-latest (request 701)."""
    result = request701.GET('/recording/data-latest')

    return result

  def page8(self):
    """GET status (request 801)."""
    result = request801.GET('/agents/status')

    return result

  def page9(self):
    """GET / (requests 901-916)."""
    result = request901.GET('/$visitors/')

    request902.GET('/download/1611262350T17n4p.jpg')

    request903.GET('/images/small/1611271010Tsbkwf.jpg')

    request904.GET('/download/1611262355Tkl91a.jpg')

    request905.GET('/img/logo.png')

    grinder.sleep(16)
    request906.GET('/images/small/1611271007Twyhfp.jpg')

    grinder.sleep(19)
    request907.GET('/images/small/1611271002T96ob6.jpg')

    grinder.sleep(49)
    request908.GET('/images/small/1611270951T7psal.jpg')

    request909.GET('/images/small/1611270954Tyii51.jpg')

    request910.GET('/images/small/1611270945Tkp1cf.jpg')

    grinder.sleep(98)
    request911.GET('/images/small/1611270925T1hqh9.jpg')

    request912.GET('/images/small/1611270935Tzzei5.jpg')

    grinder.sleep(15)
    request913.GET('/images/small/1611270920Tdyznx.jpg')

    request914.GET('/images/small/1611270915Tq9ceo.jpg')

    request915.GET('/images/small/1611270939Tnr479.jpg')

    grinder.sleep(179)
    request916.GET('/images/small/1611270911Tm8m2j.jpg')

    return result

  def page10(self):
    """GET data-latest (request 1001)."""
    result = request1001.GET('/recording/data-latest')

    return result

  def page11(self):
    """GET status (request 1101)."""
    result = request1101.GET('/agents/status')

    return result

  def page12(self):
    """GET fontawesome-webfont.woff2 (request 1201)."""
    self.token_v = \
      '4.7.0'
    result = request1201.GET('/fonts/fontawesome-webfont.woff2' +
      '?v=' +
      self.token_v)

    return result

  def page13(self):
    """GET favicon.ico (request 1301)."""
    result = request1301.GET('/favicon.ico')

    return result

  def page14(self):
    """GET status (request 1401)."""
    result = request1401.GET('/agents/status')

    return result

  def page15(self):
    """GET data-latest (request 1501)."""
    result = request1501.GET('/recording/data-latest')

    return result

  def page16(self):
    """GET / (requests 1601-1605)."""
    result = request1601.GET('/account/')

    grinder.sleep(61)
    request1602.GET('/default/js/default.js')

    request1603.GET('/widgets.css' +
      '?ts=' +
      self.token_ts)

    request1604.GET('/widgets.js' +
      '?ts=' +
      self.token_ts)

    request1605.GET('/default/css/default.css')

    return result

  def page17(self):
    """GET spa.min@14.css (requests 1701-1702)."""
    result = request1701.GET('/spa.min@14.css')

    request1702.GET('/spa.min@14.js')

    return result

  def page18(self):
    """GET status (request 1801)."""
    result = request1801.GET('/agents/status')

    return result

  def page19(self):
    """GET data-latest (request 1901)."""
    result = request1901.GET('/recording/data-latest')

    return result

  def page20(self):
    """GET / (requests 2001-2002)."""
    result = request2001.GET('/$visitors/')

    grinder.sleep(49)
    request2002.GET('/img/logo.png')

    return result

  def page21(self):
    """GET fontawesome-webfont.woff2 (request 2101)."""
    result = request2101.GET('/fonts/fontawesome-webfont.woff2' +
      '?v=' +
      self.token_v)

    return result

  def page22(self):
    """GET favicon.ico (request 2201)."""
    result = request2201.GET('/favicon.ico')

    return result

  def page23(self):
    """GET status (request 2301)."""
    result = request2301.GET('/agents/status')

    return result

  def page24(self):
    """GET data-latest (request 2401)."""
    result = request2401.GET('/recording/data-latest')

    return result

  def page25(self):
    """GET status (request 2501)."""
    result = request2501.GET('/agents/status')

    return result

  def page26(self):
    """GET data-latest (request 2601)."""
    result = request2601.GET('/recording/data-latest')

    return result

  def page27(self):
    """GET status (request 2701)."""
    result = request2701.GET('/agents/status')

    return result

  def page28(self):
    """GET data-latest (request 2801)."""
    result = request2801.GET('/recording/data-latest')

    return result

  def page29(self):
    """GET status (request 2901)."""
    result = request2901.GET('/agents/status')

    return result

  def page30(self):
    """GET data-latest (request 3001)."""
    result = request3001.GET('/recording/data-latest')

    return result

  def page31(self):
    """GET status (request 3101)."""
    result = request3101.GET('/agents/status')

    return result

  def page32(self):
    """GET data-latest (request 3201)."""
    result = request3201.GET('/recording/data-latest')

    return result

  def page33(self):
    """GET status (request 3301)."""
    result = request3301.GET('/agents/status')

    return result

  def page34(self):
    """GET data-latest (request 3401)."""
    result = request3401.GET('/recording/data-latest')

    return result

  def page35(self):
    """GET status (request 3501)."""
    result = request3501.GET('/agents/status')

    return result

  def page36(self):
    """GET data-latest (request 3601)."""
    result = request3601.GET('/recording/data-latest')

    return result

  def page37(self):
    """GET status (request 3701)."""
    result = request3701.GET('/agents/status')

    return result

  def page38(self):
    """GET data-latest (request 3801)."""
    result = request3801.GET('/recording/data-latest')

    return result

  def page39(self):
    """GET status (request 3901)."""
    result = request3901.GET('/agents/status')

    return result

  def page40(self):
    """GET data-latest (request 4001)."""
    result = request4001.GET('/recording/data-latest')

    return result

  def page41(self):
    """GET status (request 4101)."""
    result = request4101.GET('/agents/status')

    return result

  def page42(self):
    """GET data-latest (request 4201)."""
    result = request4201.GET('/recording/data-latest')

    return result

  def page43(self):
    """POST / (request 4301)."""
    result = request4301.POST('/api/account/login/',
      '{\"email\":\"hadi.safari@ut.ac.ir\",\"password\":\"p@ss\"}',
      ( NVPair('Content-Type', 'application/json; charset=utf-8'), ))

    return result

  def page44(self):
    """GET / (requests 4401-4405)."""
    result = request4401.GET('/account/')

    grinder.sleep(108)
    request4402.GET('/widgets.css' +
      '?ts=' +
      self.token_ts)

    grinder.sleep(27)
    request4403.GET('/default/css/default.css')

    request4404.GET('/widgets.js' +
      '?ts=' +
      self.token_ts)

    request4405.GET('/default/js/default.js')

    return result

  def page45(self):
    """GET status (request 4501)."""
    result = request4501.GET('/agents/status')

    return result

  def page46(self):
    """GET data-latest (request 4601)."""
    result = request4601.GET('/recording/data-latest')

    return result

  def page47(self):
    """GET spa.min@14.css (requests 4701-4702)."""
    result = request4701.GET('/spa.min@14.css')

    request4702.GET('/spa.min@14.js')

    return result

  def page48(self):
    """GET logo.png (request 4801)."""
    result = request4801.GET('/img/logo.png')

    return result

  def page49(self):
    """GET / (request 4901)."""
    result = request4901.GET('/$visitors/')

    return result

  def page50(self):
    """GET fontawesome-webfont.woff2 (request 5001)."""
    result = request5001.GET('/fonts/fontawesome-webfont.woff2' +
      '?v=' +
      self.token_v)

    return result

  def page51(self):
    """GET favicon.ico (request 5101)."""
    result = request5101.GET('/favicon.ico')

    return result

  def page52(self):
    """GET data-latest (request 5201)."""
    result = request5201.GET('/recording/data-latest')

    return result

  def page53(self):
    """GET status (request 5301)."""
    result = request5301.GET('/agents/status')

    return result

  def page54(self):
    """GET data-latest (request 5401)."""
    result = request5401.GET('/recording/data-latest')

    return result

  def page55(self):
    """GET status (request 5501)."""
    result = request5501.GET('/agents/status')

    return result

  def page56(self):
    """GET status (request 5601)."""
    result = request5601.GET('/agents/status')

    return result

  def page57(self):
    """GET data-latest (request 5701)."""
    result = request5701.GET('/recording/data-latest')

    return result

  def page58(self):
    """GET status (request 5801)."""
    result = request5801.GET('/agents/status')

    return result

  def page59(self):
    """GET data-latest (request 5901)."""
    result = request5901.GET('/recording/data-latest')

    return result

  def __call__(self):
    """Called for every run performed by the worker thread."""
    self.page1()      # GET data-latest (request 101)
    self.page2()      # GET status (request 201)

    grinder.sleep(978)
    self.page3()      # GET status (request 301)
    self.page4()      # GET data-latest (request 401)

    grinder.sleep(343)
    self.page5()      # GET / (requests 501-505)

    grinder.sleep(142)
    self.page6()      # GET spa.min@14.css (requests 601-602)

    grinder.sleep(143)
    self.page7()      # GET data-latest (request 701)
    self.page8()      # GET status (request 801)

    grinder.sleep(222)
    self.page9()      # GET / (requests 901-916)
    self.page10()     # GET data-latest (request 1001)
    self.page11()     # GET status (request 1101)
    self.page12()     # GET fontawesome-webfont.woff2 (request 1201)

    grinder.sleep(195)
    self.page13()     # GET favicon.ico (request 1301)

    grinder.sleep(241)
    self.page14()     # GET status (request 1401)
    self.page15()     # GET data-latest (request 1501)

    grinder.sleep(704)
    self.page16()     # GET / (requests 1601-1605)

    grinder.sleep(94)
    self.page17()     # GET spa.min@14.css (requests 1701-1702)
    self.page18()     # GET status (request 1801)
    self.page19()     # GET data-latest (request 1901)

    grinder.sleep(380)
    self.page20()     # GET / (requests 2001-2002)

    grinder.sleep(38)
    self.page21()     # GET fontawesome-webfont.woff2 (request 2101)

    grinder.sleep(193)
    self.page22()     # GET favicon.ico (request 2201)

    grinder.sleep(68)
    self.page23()     # GET status (request 2301)
    self.page24()     # GET data-latest (request 2401)

    grinder.sleep(852)
    self.page25()     # GET status (request 2501)
    self.page26()     # GET data-latest (request 2601)

    grinder.sleep(987)
    self.page27()     # GET status (request 2701)
    self.page28()     # GET data-latest (request 2801)

    grinder.sleep(1001)
    self.page29()     # GET status (request 2901)
    self.page30()     # GET data-latest (request 3001)

    grinder.sleep(1002)
    self.page31()     # GET status (request 3101)

    grinder.sleep(15)
    self.page32()     # GET data-latest (request 3201)

    grinder.sleep(989)
    self.page33()     # GET status (request 3301)
    self.page34()     # GET data-latest (request 3401)

    grinder.sleep(990)
    self.page35()     # GET status (request 3501)

    grinder.sleep(16)
    self.page36()     # GET data-latest (request 3601)

    grinder.sleep(983)
    self.page37()     # GET status (request 3701)
    self.page38()     # GET data-latest (request 3801)

    grinder.sleep(995)
    self.page39()     # GET status (request 3901)
    self.page40()     # GET data-latest (request 4001)

    grinder.sleep(998)
    self.page41()     # GET status (request 4101)
    self.page42()     # GET data-latest (request 4201)

    grinder.sleep(559)
    self.page43()     # POST / (request 4301)

    grinder.sleep(138)
    self.page44()     # GET / (requests 4401-4405)

    grinder.sleep(83)
    self.page45()     # GET status (request 4501)
    self.page46()     # GET data-latest (request 4601)

    grinder.sleep(15)
    self.page47()     # GET spa.min@14.css (requests 4701-4702)

    grinder.sleep(384)
    self.page48()     # GET logo.png (request 4801)
    self.page49()     # GET / (request 4901)

    grinder.sleep(151)
    self.page50()     # GET fontawesome-webfont.woff2 (request 5001)

    grinder.sleep(30)
    self.page51()     # GET favicon.ico (request 5101)

    grinder.sleep(137)
    self.page52()     # GET data-latest (request 5201)
    self.page53()     # GET status (request 5301)

    grinder.sleep(906)
    self.page54()     # GET data-latest (request 5401)
    self.page55()     # GET status (request 5501)

    grinder.sleep(927)
    self.page56()     # GET status (request 5601)
    self.page57()     # GET data-latest (request 5701)

    grinder.sleep(1000)
    self.page58()     # GET status (request 5801)
    self.page59()     # GET data-latest (request 5901)


# Instrument page methods.
Test(100, 'Page 1').record(TestRunner.page1)
Test(200, 'Page 2').record(TestRunner.page2)
Test(300, 'Page 3').record(TestRunner.page3)
Test(400, 'Page 4').record(TestRunner.page4)
Test(500, 'Page 5').record(TestRunner.page5)
Test(600, 'Page 6').record(TestRunner.page6)
Test(700, 'Page 7').record(TestRunner.page7)
Test(800, 'Page 8').record(TestRunner.page8)
Test(900, 'Page 9').record(TestRunner.page9)
Test(1000, 'Page 10').record(TestRunner.page10)
Test(1100, 'Page 11').record(TestRunner.page11)
Test(1200, 'Page 12').record(TestRunner.page12)
Test(1300, 'Page 13').record(TestRunner.page13)
Test(1400, 'Page 14').record(TestRunner.page14)
Test(1500, 'Page 15').record(TestRunner.page15)
Test(1600, 'Page 16').record(TestRunner.page16)
Test(1700, 'Page 17').record(TestRunner.page17)
Test(1800, 'Page 18').record(TestRunner.page18)
Test(1900, 'Page 19').record(TestRunner.page19)
Test(2000, 'Page 20').record(TestRunner.page20)
Test(2100, 'Page 21').record(TestRunner.page21)
Test(2200, 'Page 22').record(TestRunner.page22)
Test(2300, 'Page 23').record(TestRunner.page23)
Test(2400, 'Page 24').record(TestRunner.page24)
Test(2500, 'Page 25').record(TestRunner.page25)
Test(2600, 'Page 26').record(TestRunner.page26)
Test(2700, 'Page 27').record(TestRunner.page27)
Test(2800, 'Page 28').record(TestRunner.page28)
Test(2900, 'Page 29').record(TestRunner.page29)
Test(3000, 'Page 30').record(TestRunner.page30)
Test(3100, 'Page 31').record(TestRunner.page31)
Test(3200, 'Page 32').record(TestRunner.page32)
Test(3300, 'Page 33').record(TestRunner.page33)
Test(3400, 'Page 34').record(TestRunner.page34)
Test(3500, 'Page 35').record(TestRunner.page35)
Test(3600, 'Page 36').record(TestRunner.page36)
Test(3700, 'Page 37').record(TestRunner.page37)
Test(3800, 'Page 38').record(TestRunner.page38)
Test(3900, 'Page 39').record(TestRunner.page39)
Test(4000, 'Page 40').record(TestRunner.page40)
Test(4100, 'Page 41').record(TestRunner.page41)
Test(4200, 'Page 42').record(TestRunner.page42)
Test(4300, 'Page 43').record(TestRunner.page43)
Test(4400, 'Page 44').record(TestRunner.page44)
Test(4500, 'Page 45').record(TestRunner.page45)
Test(4600, 'Page 46').record(TestRunner.page46)
Test(4700, 'Page 47').record(TestRunner.page47)
Test(4800, 'Page 48').record(TestRunner.page48)
Test(4900, 'Page 49').record(TestRunner.page49)
Test(5000, 'Page 50').record(TestRunner.page50)
Test(5100, 'Page 51').record(TestRunner.page51)
Test(5200, 'Page 52').record(TestRunner.page52)
Test(5300, 'Page 53').record(TestRunner.page53)
Test(5400, 'Page 54').record(TestRunner.page54)
Test(5500, 'Page 55').record(TestRunner.page55)
Test(5600, 'Page 56').record(TestRunner.page56)
Test(5700, 'Page 57').record(TestRunner.page57)
Test(5800, 'Page 58').record(TestRunner.page58)
Test(5900, 'Page 59').record(TestRunner.page59)
