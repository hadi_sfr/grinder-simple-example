#!/usr/bin/env python3

import csv
from sys import argv, stderr
from collections import defaultdict


def main():
    files = argv[1:]
    requests_per_second = defaultdict(int)
    response_times = []
    for file in files:
        print("Processing %s…" % file, file=stderr)
        with open(file) as fd:
            reader = csv.DictReader(fd)
            for row in reader:
                response_time = int(row[' Time to first byte'])
                done_time = (int(row[' Start time (ms since Epoch)']) + response_time) // 1000
                if response_time:
                    response_times.append(response_time)
                    requests_per_second[done_time] += 1
    print("", file=stderr)
    avg_response_time = sum(response_times) / len(response_times)
    max_response_time = max(response_times)
    max_throughput = max(requests_per_second.values())
    avg_throughput = sum(requests_per_second.values()) / len(requests_per_second)
    print("Avg Response Time: %s (ms)" % avg_response_time)
    print("Max Response Time: %s (ms)" % max_response_time)
    print("Max Throughput: %s (requests per second)" % max_throughput)
    print("Avg Throughput: %s (requests per second)" % avg_throughput)


if __name__ == '__main__':
    main()
