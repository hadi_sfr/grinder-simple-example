# The Grinder 3.11
# HTTP script recorded by har2grinder

from net.grinder.script import Test
from net.grinder.script.Grinder import grinder
from net.grinder.plugin.http import HTTPPluginControl, HTTPRequest
from HTTPClient import NVPair
connectionDefaults = HTTPPluginControl.getConnectionDefaults()
httpUtilities = HTTPPluginControl.getHTTPUtilities()

# To use a proxy server, uncomment the next line and set the host and port.
# connectionDefaults.setProxyServer("localhost", 8001)


def createRequest(test, url, headers=None):
        request = HTTPRequest(url=url)
        if headers:
            request.headers = headers
        test.record(request, HTTPRequest.getHttpMethodFilter())
        return request


# These definitions at the top level of the file are evaluated once,
# when the worker process is started.

connectionDefaults.defaultHeaders = []

# without this, the grinder seems to remove the cookies we might have in
# our headers.
# http://grinder.sourceforge.net/g3/http-plugin.html
connectionDefaults.useCookies = 0

# HEADER LIBRARY SECTION
header_lib = [
    NVPair('Host', 'acm.ut.ac.ir:8888'),  # 0
    NVPair('Connection', 'keep-alive'),  # 1
    NVPair('Pragma', 'no-cache'),  # 2
    NVPair('Cache-Control', 'no-cache'),  # 3
    NVPair('Upgrade-Insecure-Requests', '1'),  # 4
    NVPair('User-Agent', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36'),  # 5
    NVPair('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8'),  # 6
    NVPair('Referer', 'http://acm.ut.ac.ir:8888/account/'),  # 7
    NVPair('Accept-Encoding', 'gzip, deflate'),  # 8
    NVPair('Accept-Language', 'en-GB,en;q=0.9,fa;q=0.8'),  # 9
    NVPair('Cookie', '_ga=GA1.3.399930531.1538323182; visit=154624055956103243; __user=14216-39353d7536617d20667964752b2072227c732120777777247537367d76366f362f666372677227343329350f2e193a2b76726130736f662d27647e312078207f7120257173712474737760'),  # 10
    NVPair('Accept', 'text/css,*/*;q=0.1'),  # 11
    NVPair('Accept', '*/*'),  # 12
    NVPair('Accept', 'image/webp,image/apng,image/*,*/*;q=0.8'),  # 13
    NVPair('x-referrer', 'http://acm.ut.ac.ir:8888/account/'),  # 14
    NVPair('x-ping', '/account/'),  # 15
    NVPair('X-Requested-With', 'XMLHttpRequest'),  # 16
    NVPair('x-cookies', '1'),  # 17
    NVPair('Referer', 'http://cdn.componentator.com/spa.min@14.css'),  # 18
    NVPair('Origin', 'http://acm.ut.ac.ir:8888'),  # 19
    NVPair('Referer', 'https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,200,700'),  # 20
    NVPair('Cookie', '_ga=GA1.3.399930531.1538323182; __user=14216-39353d7536617d20667964752b2072227c732120777777247537367d76366f362f666372677227343329350f2e193a2b76726130736f662d27647e312078207f7120257173712474737760; visit=154624065723103243'),  # 21
]


# HEADERS SECTION
headers1001 = [
    header_lib[0],
    header_lib[1],
    header_lib[2],
    header_lib[3],
    header_lib[4],
    header_lib[5],
    header_lib[6],
    header_lib[7],
    header_lib[8],
    header_lib[9],
    header_lib[10],
]
headers1002 = [
    header_lib[7],
    header_lib[5],
]
headers1003 = [
    header_lib[7],
    header_lib[5],
]
headers1004 = [
    header_lib[2],
    header_lib[8],
    header_lib[0],
    header_lib[9],
    header_lib[5],
    header_lib[11],
    header_lib[7],
    header_lib[10],
    header_lib[1],
    header_lib[3],
]
headers1005 = [
    header_lib[7],
    header_lib[5],
]
headers1006 = [
    header_lib[2],
    header_lib[8],
    header_lib[0],
    header_lib[9],
    header_lib[5],
    header_lib[11],
    header_lib[7],
    header_lib[10],
    header_lib[1],
    header_lib[3],
]
headers1007 = [
    header_lib[2],
    header_lib[8],
    header_lib[0],
    header_lib[9],
    header_lib[5],
    header_lib[12],
    header_lib[7],
    header_lib[10],
    header_lib[1],
    header_lib[3],
]
headers1008 = [
    header_lib[2],
    header_lib[8],
    header_lib[0],
    header_lib[9],
    header_lib[5],
    header_lib[13],
    header_lib[7],
    header_lib[10],
    header_lib[1],
    header_lib[3],
]
headers1009 = [
    header_lib[2],
    header_lib[8],
    header_lib[0],
    header_lib[9],
    header_lib[5],
    header_lib[12],
    header_lib[7],
    header_lib[10],
    header_lib[1],
    header_lib[3],
]
headers1010 = [
    header_lib[7],
    header_lib[5],
]
headers1011 = [
    header_lib[2],
    header_lib[8],
    header_lib[14],
    header_lib[9],
    header_lib[0],
    header_lib[5],
    header_lib[12],
    header_lib[15],
    header_lib[3],
    header_lib[16],
    header_lib[10],
    header_lib[1],
    header_lib[7],
    header_lib[17],
]
headers1012 = [
    header_lib[5],
    header_lib[18],
    header_lib[19],
]
headers1013 = [
    header_lib[5],
    header_lib[20],
    header_lib[19],
]
headers1014 = [
    header_lib[2],
    header_lib[8],
    header_lib[0],
    header_lib[9],
    header_lib[5],
    header_lib[12],
    header_lib[7],
    header_lib[16],
    header_lib[21],
    header_lib[1],
    header_lib[3],
]


# REQUESTS SECTION
request1001 = createRequest(Test(1001, 'GET /account/'), 'http://acm.ut.ac.ir:8888', headers1001)
request1002 = createRequest(Test(1002, 'GET /spa.min@14.css'), 'http://cdn.componentator.com', headers1002)
request1003 = createRequest(Test(1003, 'GET /css'), 'http://fonts.googleapis.com', headers1003)
request1004 = createRequest(Test(1004, 'GET /widgets.css'), 'http://acm.ut.ac.ir:8888', headers1004)
request1005 = createRequest(Test(1005, 'GET /spa.min@14.js'), 'http://cdn.componentator.com', headers1005)
request1006 = createRequest(Test(1006, 'GET /default/css/default.css'), 'http://acm.ut.ac.ir:8888', headers1006)
request1007 = createRequest(Test(1007, 'GET /default/js/default.js'), 'http://acm.ut.ac.ir:8888', headers1007)
request1008 = createRequest(Test(1008, 'GET /img/logo.png'), 'http://acm.ut.ac.ir:8888', headers1008)
request1009 = createRequest(Test(1009, 'GET /widgets.js'), 'http://acm.ut.ac.ir:8888', headers1009)
request1010 = createRequest(Test(1010, 'GET /css'), 'https://fonts.googleapis.com', headers1010)
request1011 = createRequest(Test(1011, 'GET /$visitors/'), 'http://acm.ut.ac.ir:8888', headers1011)
request1012 = createRequest(Test(1012, 'GET /fonts/fontawesome-webfont.woff2'), 'http://cdn.componentator.com', headers1012)
request1013 = createRequest(Test(1013, 'GET /s/sourcesanspro/v11/6xKydSBYKcSV-LCoeQqfX1RYOo3ig4vwlxdu3cOWxw.woff2'), 'https://fonts.gstatic.com', headers1013)
request1014 = createRequest(Test(1014, 'GET /api/account/orders/'), 'http://acm.ut.ac.ir:8888', headers1014)


class TestRunner:
        """A TestRunner instance is created for each worker thread."""
        def page1(self):
                request1001.GET('account/')
                request1002.GET('spa.min@14.css')
                request1003.GET('css?family=Source+Sans+Pro:400,200,700')
                request1004.GET('widgets.css?ts=1avy1')
                request1005.GET('spa.min@14.js')
                request1006.GET('default/css/default.css')
                request1007.GET('default/js/default.js')
                request1008.GET('img/logo.png')
                request1009.GET('widgets.js?ts=1avy1')
                request1010.GET('css?family=Source+Sans+Pro:400,200,700')
                request1011.GET('$visitors/')
                request1012.GET('fonts/fontawesome-webfont.woff2?v=4.7.0')
                request1013.GET('s/sourcesanspro/v11/6xKydSBYKcSV-LCoeQqfX1RYOo3ig4vwlxdu3cOWxw.woff2')
                request1014.GET('api/account/orders/')

        def __call__(self):
                """Called for every run performed by the worker thread."""
                self.page1()


# Instrument page methods.
Test(1000, 'page_3').record(TestRunner.page1)
